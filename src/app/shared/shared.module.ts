import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';

import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import { CommonOperationComponent } from './common-operation/common-operation.component';




@NgModule({
  declarations: [HeaderComponent,FooterComponent, CommonOperationComponent],
  imports: [
    CommonModule,
    ModalModule.forRoot()
  ],
  exports:[HeaderComponent,FooterComponent,CommonOperationComponent]
})
export class SharedModule { }
