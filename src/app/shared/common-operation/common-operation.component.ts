import { Component, OnInit, Input } from '@angular/core';
import { first } from 'rxjs/operators';

import {AddshopComponent} from '../../components/addshop/addshop.component';
import { BsModalService,BsModalRef } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-common-operation',
  templateUrl: './common-operation.component.html',
  styleUrls: ['./common-operation.component.scss']
})
export class CommonOperationComponent implements OnInit {

  public modalRef: BsModalRef;

  @Input() addData;
  constructor(
    private modalService: BsModalService
  ) { }

  ngOnInit() {
  }
  openModal() {
    this.modalRef = this.modalService.show(AddshopComponent);
        this.modalRef.content.onClose.subscribe(result => {
            console.log('results', result);
        })

  }
}
