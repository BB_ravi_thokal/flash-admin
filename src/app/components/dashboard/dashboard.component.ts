import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  adminCards:any;
  constructor(
    private router:Router
  ) { }

  ngOnInit() {
    this.adminCards=[{
      name:"Shop",
      route:"shop"
    },
    {
      name:"User",
      // route:"user"
    },
    {
      name:"Category",
      // route:"category"
    },
    {
      name:"Menu",
      // route:"menu"
    },
    {
      name:"Advertise Banners",
      // route:"advertise"
    },
    {
      name:"Offers",
      // route:"offer"
    },
    {
      name:"Delivery Boy ",
      // route:"deliveryboy"
    },
    {
      name:"Other Configue",
      // route:"otherconfigue"
    }
  ]
  }
  gotoCard(route){
    console.log(route);
    this.router.navigate([`${route}`])
  }
}
