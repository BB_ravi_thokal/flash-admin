import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import { ShopComponent } from './shop/shop.component';


const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path:'dashboard',
    component:DashboardComponent
  },
  {
    path:'shop',
    component:ShopComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
