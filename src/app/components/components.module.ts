import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";


import { ComponentsRoutingModule } from './components-routing.module';

import {SharedModule} from '../shared/shared.module';
import { KeysPipe } from '../core/pipes/keys.pipe';

import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import { ShopComponent } from './shop/shop.component';
import { AddshopComponent } from './addshop/addshop.component';


@NgModule({
  declarations: [DashboardComponent,LoginComponent, ShopComponent,KeysPipe, AddshopComponent],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    SharedModule,
    FormsModule, 
    ReactiveFormsModule,
    NgxSpinnerModule,
    // BrowserAnimationsModule,
    // ToastrModule.forRoot()
  ],
  exports:[AddshopComponent],
  entryComponents:[AddshopComponent]
})
export class ComponentsModule { }
