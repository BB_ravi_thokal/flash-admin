import { Component, OnInit} from '@angular/core';
import { BsModalService,BsModalRef } from 'ngx-bootstrap/modal';

import { ShopService } from 'src/app/core/services/shop.service';
import { AddshopComponent } from '../addshop/addshop.component';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})

export class ShopComponent implements OnInit {

  public modalRef: BsModalRef;
  
  items=[];
  shopData;
  addShop="shop";
  constructor(
    private shopService:ShopService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.callShops();
  }

  callShops(){
    this.shopData=[];
    let getShops={
      pageNumber:1,
      filterBy:"",
      searchData:""
    }
    this.shopService.getShopsData$(getShops).subscribe({
      next: (results) => {
        this.shopData=results.result;
        this.shopData.forEach(element => {
          let shopObject={
            shopId:element.shopId,
            shopName:element.shopName,
            categoryName:element.categoryDetails.categoryName,
            shopOwnerName:element.shopOwnerName,
            shopContactNumber:element.shopContactNumber,
            shopEmail:element.shopEmail,
            shopAdress:element.shopAdress
          }
          this.items.push(shopObject);
        });
        console.log(results,"shop");                
      }, error: error => {
        console.log(error,"error");                     
      }
    }); 
  }
  deleteShop(shopId){
  console.log(shopId,"shopId");
  let deleteShop={
    shopId:shopId,
    isDeleted:true
  }
  this.shopService.deleteShopData$(deleteShop).subscribe({
    next: (results) => {
      //this.items=results.result;
      console.log(results,"shop");  
      if(results.status==0){
        this.callShops()
      }              
    }, error: error => {
      console.log(error,"error");                     
    }
  }); 
  }
  editShop(shopId){
    this.shopData.forEach(element => {
      if(element.shopId==shopId){
        console.log(element,"element"); 
        this.shopService.editShop=element;       
      }
    });
    this.modalRef = this.modalService.show(AddshopComponent);
        this.modalRef.content.onClose.subscribe(result => {
            console.log('results', result);
            this.shopService.editShop=undefined;
        })
  }
}
