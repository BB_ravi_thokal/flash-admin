import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormBuilder,FormGroup,FormControl,Validators} from '@angular/forms';

//import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";



import { LoginService } from 'src/app/core/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  constructor(
    private router:Router,
    private formBuilder:FormBuilder,
    private loginService:LoginService,
    //private spinner: NgxSpinnerService
    //private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      adminNumber: new FormControl(
        '',
        Validators.compose([
          //fetch value from userid field and validate it
          Validators.required,
          Validators.pattern('^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$')
        ])
      ),
      adminPassword: new FormControl(
        '',
        Validators.compose([
          //fetch value from password feild and validate it
          Validators.required,
        ])
      )
    });
    // this.spinner.show();
 
    // setTimeout(() => {
    //   /** spinner ends after 5 seconds */
    //   this.spinner.hide();
    // }, 5000);
  
  }
  get f() { return this.loginForm.controls; }
  loginAdmin(){
    this.submitted = true;

        // stop here if form is valid
        if (this.loginForm.valid) {
          let number=this.loginForm.get('adminNumber').value;
          let password=this.loginForm.get('adminPassword').value;
          

          let loginData={
            contactNumber:number,
            password:password
          }
          this.loginService.adminLogin$(loginData).subscribe({
            next: (results) => {
              console.log(results,"login");
              if(results.status==0){
                //this.toastr.success('Hello world!', 'Toastr fun!');
                this.router.navigate(['dashboard']);
              }              
            }, error: error => {
              console.log(error,"error");
                           
            }
          });          
        }
        else{
          return;
        }
    
  }

}
