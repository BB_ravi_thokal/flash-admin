import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from '@angular/forms';
import { Subject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { ShopService } from 'src/app/core/services/shop.service';



@Component({
  selector: 'app-addshop',
  templateUrl: './addshop.component.html',
  styleUrls: ['./addshop.component.scss']
})
export class AddshopComponent implements OnInit {

  public onClose: Subject<boolean>;

  shopForm: FormGroup;
  submitted = false;
  categoryName: string="Select";
  categoryData:any;
  categoryId;
  editShop;
  addUpdateFlag:boolean=false;
  miniLogoError;
  logoError;
  categoryError;
  constructor(
    private formBuilder:FormBuilder,
    private _bsModalRef: BsModalRef,
    private shopService:ShopService,
    private detectChange:ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.editShop=this.shopService.editShop;
    console.log(this.editShop,"edit");
    
    this.onClose = new Subject();
    this.getCategory();
    this.shopForm = this.formBuilder.group({
      category: new FormControl(
        'Select',
        Validators.compose([
          //Validators.required        
        ])
      ),
      shopName: new FormControl(
        '',
        Validators.compose([
          //Validators.required,
        ])
      ),
      shopDesp: new FormControl(
        '',
        Validators.compose([
          //Validators.required,
        ])
      ),
      shopOwner: new FormControl(
        '',
        Validators.compose([
          //Validators.required,
        ])
      ),
      mobileNumber: new FormControl(
        '',
        Validators.compose([
          //Validators.required,
        ])
      ),
      ownerEmail: new FormControl(
        '',
        Validators.compose([
          //Validators.required,
        ])
      ),
      shopAddress: new FormControl(
        '',
        Validators.compose([
          //Validators.required,
        ])
      ),
      shopRating: new FormControl(
        '',
        Validators.compose([
          //Validators.required,
        ])
      ),
      longitude: new FormControl(
        '',
        Validators.compose([
          //Validators.required,
        ])
      ),
      latitude: new FormControl(
        '',
        Validators.compose([
          //Validators.required,
        ])
      ),
      shopLogo: new FormControl(
        '',
        Validators.compose([
          //Validators.required,
        ])
      ),
      miniLogo: new FormControl(
        '',
        Validators.compose([
          //Validators.required,
        ])
      ),
    });
    if(this.editShop!=undefined){
      this.shopForm.controls['category'].setValue(this.editShop.categoryDetails.categoryName);
      this.shopForm.controls['shopName'].setValue(this.editShop.shopName);
      this.shopForm.controls['shopDesp'].setValue(this.editShop.shopDescription);
      this.shopForm.controls['shopOwner'].setValue(this.editShop.shopOwnerName);
      this.shopForm.controls['mobileNumber'].setValue(this.editShop.shopContactNumber);
      this.shopForm.controls['ownerEmail'].setValue(this.editShop.shopEmail);
      this.shopForm.controls['shopAddress'].setValue(this.editShop.ishopAdressd);
      this.shopForm.controls['shopRating'].setValue(this.editShop.shopRating);
      this.shopForm.controls['longitude'].setValue(this.editShop.loc[1]);
      this.shopForm.controls['latitude'].setValue(this.editShop.loc[0]);
      //this.shopForm.controls['shopLogo'].setValue(this.editShop.shopLogo);
      //this.shopForm.controls['miniLogo'].setValue(this.editShop.shopMiniLogo);

      this.addUpdateFlag=true;
    }
    else{
      this.shopForm.controls['category'].setValue("Select");
      this.shopForm.controls['shopName'].setValue("");
      this.shopForm.controls['shopDesp'].setValue("");
      this.shopForm.controls['shopOwner'].setValue("");
      this.shopForm.controls['mobileNumber'].setValue("");
      this.shopForm.controls['ownerEmail'].setValue("");
      this.shopForm.controls['shopAddress'].setValue("");
      this.shopForm.controls['shopRating'].setValue("");
      this.shopForm.controls['longitude'].setValue("");
      this.shopForm.controls['latitude'].setValue("");
    }
  }
  closeModal(){
    this.addUpdateFlag=false;
    this.editShop=undefined;
    this.onClose.next(false);
    this._bsModalRef.hide();
    this.detectChange.checkNoChanges();
  }
  get f() { return this.shopForm.controls; }
  getCategory(){
    this.shopService.getCategoryData$().subscribe({
      next: (results) => {
        this.categoryData=results.result
        console.log(results,"shop");
                
      }, error: error => {
        console.log(error,"error");
                     
      }
    }); 
  }
  getUserId(event){
    this.categoryName=event ;
    this.categoryData.forEach(element => {
      if(element.categoryName==event){
        this.categoryId=element.categoryId;
      }
    });       
  }
  addData(){
    this.submitted = true;
    // stop here if form is valid
    let logo=this.shopForm.get('shopLogo').value==null;
    let miniLogo=this.shopForm.get('miniLogo').value==null;
    let category=this.shopForm.get('category').value;
    if(category=="Select"){
      this.categoryError=true;
    }
    if(logo==false){
      this.logoError=true;
    }
    if(miniLogo==false){
      this.miniLogoError=true;
    }    
    if (this.shopForm.valid) {
        if(this.addUpdateFlag==false){

          let addShop={
            shopName: this.shopForm.get('miniLogo').value,
            categoryId:this.categoryId,
            shopDescription: this.shopForm.get('miniLogo').value,
            //shopMiniLogo:this.shopForm.get('miniLogo').value,
            //shopLogo:this.shopForm.get('shopLogo').value,
            shopMiniLogo:"https://flash-catagory-image.s3.ap-south-1.amazonaws.com/defaultShop.jpeg",
            shopLogo:"https://flash-catagory-image.s3.ap-south-1.amazonaws.com/defaultShop.jpeg",
            shopRating: this.shopForm.get('miniLogo').value, //add this default
            shopOwnerName: this.shopForm.get('miniLogo').value,
            shopContactNumber: this.shopForm.get('miniLogo').value,
            shopEmail: this.shopForm.get('miniLogo').value,
            shopAdress: this.shopForm.get('miniLogo').value,
            shopLocation: { 
                            longitudeDelta: 0.001, 
                            latitudeDelta: 0.001,
                            longitude: this.shopForm.get('longitude').value,
                            latitude: this.shopForm.get('latitude').value
                              },
            loc: [ this.shopForm.get('latitude').value, this.shopForm.get('longitude').value],  
            }

          this.shopService.addShopData$(addShop).subscribe({
            next: (results) => {
              console.log(results,"login");
              if(results.status=200){
                //this.toastr.success('Hello world!', 'Toastr fun!');
                this.onClose.next(false);
                this._bsModalRef.hide();
                this.editShop=undefined;
                this.addUpdateFlag=false;
              }              
            }, error: error => {
              console.log(error,"error");                         
            }
          }); 
        }
         if(this.addUpdateFlag==true){

          let addShop={
            shopName: this.shopForm.get('miniLogo').value,
            categoryId:this.categoryId,
            shopDescription: this.shopForm.get('miniLogo').value,
            //shopMiniLogo:this.shopForm.get('miniLogo').value,
            //shopLogo:this.shopForm.get('shopLogo').value,
            shopMiniLogo:"https://flash-catagory-image.s3.ap-south-1.amazonaws.com/defaultShop.jpeg",
            shopLogo:"https://flash-catagory-image.s3.ap-south-1.amazonaws.com/defaultShop.jpeg",
            shopRating: this.shopForm.get('miniLogo').value, //add this default
            shopOwnerName: this.shopForm.get('miniLogo').value,
            shopContactNumber: this.shopForm.get('miniLogo').value,
            shopEmail: this.shopForm.get('miniLogo').value,
            shopAdress: this.shopForm.get('miniLogo').value,
            shopLocation: { 
                            longitudeDelta: 0.001, 
                            latitudeDelta: 0.001,
                            longitude: this.shopForm.get('longitude').value,
                            latitude: this.shopForm.get('latitude').value
                              },
            loc: [ this.shopForm.get('latitude').value, this.shopForm.get('longitude').value], 
            shopId:this.editShop.shopId
            }

          this.shopService.updateShopData$(addShop).subscribe({
            next: (results) => {
              console.log(results,"login");
              if(results.status=200){
                //this.toastr.success('Hello world!', 'Toastr fun!');
                this.onClose.next(false);
                this._bsModalRef.hide();
                this.editShop=undefined;
                this.addUpdateFlag=false;
              }              
            }, error: error => {
              console.log(error,"error");                         
            }
          });  
         }
    }
    else{
      return
    }
  }
}
