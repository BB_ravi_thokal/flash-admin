import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  editShop;
  constructor(
    private http: HttpClient
  ) { }

  /**
   * getShopsData$ fn is use to get shop data
   */
  public getShopsData$(getShop): Observable<any> {    
    const url = `${environment.apiBaseUrl}/api/user/get-shop`;
    return this.http.post<any>(url,getShop);
  }

  /**
   * getCategoryData$ fn is use to get category data
   */
  public getCategoryData$(): Observable<any> { 

    const url = `${environment.apiBaseUrl}/api/categories/get-categories`;
    return this.http.get<any>(url);
  }

  /**
   * addShopData$ fn is use to add shop 
   */
  public addShopData$(shopData): Observable<any> {
    const url = `${environment.apiBaseUrl}/api/user/create-shop`;
    return this.http.post<any>(url, shopData);
  }

  /**
   * deleteShopData$ fn is use to delete shop 
   */
  public deleteShopData$(shopData): Observable<any> {
    const url = `${environment.apiBaseUrl}/api/user/update-shop-details`;
    return this.http.post<any>(url, shopData);
  }

  /**
   * updateShopData$ fn is use to add shop 
   */
  public updateShopData$(shopData): Observable<any> {
    const url = `${environment.apiBaseUrl}/api/user/update-shop-details`;
    return this.http.post<any>(url, shopData);
  }
}
