import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  public adminLogin$(loginData): Observable<any> {
    const url = `${environment.apiBaseUrl}/api/user/login`;
    return this.http.post<any>(url, loginData);
  }
}
